> [فایل فارسی](./README.fa.md)

# Judging-Professors

A place to judge, grade and review professors of Shiraz university's academic (and sometimes non-academic) behaviors by students.

# What to expect

As there are different opinions regarding "judgment":

1. **Objective judgment**: Some debate that judging people is wrong altogether. On the other hand, it is believed by others that, judgment is the only tool of the weak. 
For this reason, this project aims to judge the staff _as objectively as possible_ to avoid misjudgment and still use this wonderful tool.

2. **Subjective Characteristics**: It is said that some characteristics of professors, e.g. their teaching method, are opinion based and because not everyone agrees with one characteristic as good they are not to be judged. 
We aim to separate this subjective characteristics as much as possible with different literature and presented as facts and information.

3. **Contribution**: It is not easy to gauge the extend of how big this project will be in the future. Hence it is recommended for each person to add their personal judgment in a exclusive branch. 
A list of branch ~~is~~ will be available at [branch-list.md](./brach-list.md "local file").
  - [ ] TODO: Add a guideline for contribution.

4. **Ethics**: We must note that this project is only to judge the professors in a just, ethical, and transparent about their academic abilities and academic ethics. Therefore it is highly recommended to avoid disclosure of their personal information and such. The responsibility of any written content is hence by its respected author.
